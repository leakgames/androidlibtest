package com.addcomponent.unitynativeplugin;

import android.os.Bundle;
import android.util.Log;

import com.unity3d.player.UnityPlayerActivity;

/**
 * Created by Max on 2/7/2018.
 */

public class MainActivity extends UnityPlayerActivity {
    @Override
    protected void onCreate(Bundle bundle) {
        Log.d("PLUGIN", "on create");
        super.onCreate(bundle);
    }
}
