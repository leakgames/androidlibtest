﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PluginTest : MonoBehaviour {

    //private AndroidJavaObject toastExample = null;
    //private AndroidJavaObject activityContext = null;

    //private AndroidJavaClass pluginClass = null;


    AndroidJavaClass jc;
    string javaMessage = "";

    void Start()
    {
        // Acces the android java receiver we made
        jc = new AndroidJavaClass("com.leak.brodcastlib.MyReceiver");
        // We call our java class function to create our MyReceiver java object
        jc.CallStatic("createInstance");
    }

    void Update()
    {
        // We get the text property of our receiver
        javaMessage = jc.GetStatic<string>("text");
        if (!string.IsNullOrEmpty(javaMessage)) Debug.Log(javaMessage);
    }


    //void Start()
    //{

    //    //pluginClass = new AndroidJavaClass("com.leak.androidtest.Helper");
    //    //pluginClass.CallStatic("init");

    //    //AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    //    //activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");

    //    //if (pluginClass != null)
    //    //{
    //    //    toastExample = pluginClass.GetStatic<AndroidJavaObject>("instance");
    //    //    //toastExample = pluginClass.CallStatic<AndroidJavaObject>("GetInstance");
    //    //    toastExample.Call("SetContext", activityContext);
    //    //    //activityContext.Call("runOnUIThread", new AndroidJavaRunnable(() =>
    //    //    //{
    //    //    //    toastExample.Call("showMessage", "This is a Toast message");
    //    //    //}));
    //    //}
    //    //else
    //    //{
    //    //    Debug.LogError("Class is null");
    //    //}
    //}

    //public void Show()
    //{
    //    AndroidJavaClass ajc = new AndroidJavaClass("com.leak.broadcastlib.Bridge");
    //    ajc.CallStatic("ShowCamera", 1234);


    //    //using (pluginClass)
    //    //{
    //    //    //AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    //    //    //activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
    //    //    activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() =>
    //    //    {
    //    //        toastExample.Call("ShowMessage", "This is a Toast message");
    //    //    }));
    //    //}

    //    ////AndroidJavaClass customActivityClass = new AndroidJavaClass("com.leak.mylibrary.MyCustomActivity");
    //    ////int val = customActivityClass.CallStatic<int>("callMe");
    //    //////int inited = customActivityClass.GetStatic<int>("val");

    //    ////Debug.LogError("customActivityClass inited " + val);

    //}
}
